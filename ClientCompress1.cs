﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Sockets;
using System.IO;
using System.IO.Compression;
using System.Text;
using System.Linq;
using System.Security;
using System.Threading.Tasks;
using System.Security.Cryptography;
using System.Runtime.InteropServices;

namespace ClientCrypt
{
    class Program
    {
        static void Main(string[] args)
        {
            TcpClient tcpClient = new TcpClient("127.0.0.1", 8080);
            Console.WriteLine("Connected To FTP Server");
            try
            {
                string Password = "GT18";
                string zipPath = @".\Compressed.zip";
                string encryptPath = @".\Encrypted.zip";

                StreamWriter sWriter = new StreamWriter(tcpClient.GetStream());
                Console.WriteLine("Enter a full file path");
                string fileName = Console.ReadLine();
                sWriter.WriteLine(Password);
                string startPath = fileName;
                sWriter.Flush();

                ZipFile.CreateFromDirectory(startPath, zipPath);
                Console.WriteLine("File Compressed in : " + Environment.CurrentDirectory);

                EncryptFile(zipPath, encryptPath, Password);
                Console.WriteLine("Your file is now encrypted");
                Console.WriteLine("Press Enter to Exit");

                byte[] bytes = File.ReadAllBytes(encryptPath);
            
                sWriter.WriteLine(bytes.Length.ToString());
                sWriter.Flush();
                sWriter.WriteLine(encryptPath);
                sWriter.Flush();

                Console.WriteLine("Sending the file...");
                tcpClient.Client.SendFile(encryptPath);
            }
            catch (Exception x)
            {
                Console.Write(x.Message);
            }
            Console.Read();
        }

        public static byte[] AES_Encrypt(byte[] bytesToBeEncrypted, byte[] Password)
        {
            byte[] EncrypBiytes = null;
            byte[] saltBytes = new byte[] { 1, 2, 3, 4, 5, 6, 7, 8 };

            using (MemoryStream Memory = new MemoryStream())
            {
                using (RijndaelManaged AES = new RijndaelManaged())
                {
                    AES.KeySize = 256;
                    AES.BlockSize = 128;

                    var Key = new Rfc2898DeriveBytes(Password, saltBytes, 1000);
                    AES.Key = Key.GetBytes(AES.KeySize / 8);
                    AES.IV = Key.GetBytes(AES.BlockSize / 8);

                    AES.Mode = CipherMode.CBC;

                    using (var cs = new CryptoStream(Memory, AES.CreateEncryptor(), CryptoStreamMode.Write))
                    {
                        cs.Write(bytesToBeEncrypted, 0, bytesToBeEncrypted.Length);
                        cs.Close();
                    }
                    EncrypBiytes = Memory.ToArray();
                }
            }
            return EncrypBiytes;
        }

        public static void EncryptFile(string file, string fileEncrypted, string password)
        {
            byte[] bytesToBeEncrypted = File.ReadAllBytes(file);
            byte[] passwordBytes = Encoding.UTF8.GetBytes(password);

            passwordBytes = SHA256.Create().ComputeHash(passwordBytes);
            byte[] bytesEncrypted = AES_Encrypt(bytesToBeEncrypted, passwordBytes);

            File.WriteAllBytes(fileEncrypted, bytesEncrypted);
        }
    }
}