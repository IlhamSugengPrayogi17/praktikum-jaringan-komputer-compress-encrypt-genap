# Praktikum Jaringan Komputer Compress Encrypt Genap 

Name    : Ilham Sugeng Prayogi

NRP     : 4210181022

**PRAKTIKUM PEMROGRAMAN JARINGAN KOMPUTER**

**Description**
- First, User will input file path in program
- Then, ClientCompress program will compress and saved in Client dictionary
- After file compressed, it will be Encrypt and saved in Client dictionary
- Encrypted file will send to Server
- Server will receive encrypt file and save it
- Next, encrypt file will decrypt and saved in directory
- DEcrypt file will be decompressed, and saved in a new folder

**Flow**

Compress -> Encrypt -> Send -> Decrypt -> Decompress