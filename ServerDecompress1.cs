﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Sockets;
using System.IO;
using System.IO.Compression;
using System.Text;
using System.Linq;
using System.Security;
using System.Threading.Tasks;
using System.Security.Cryptography;
using System.Runtime.InteropServices;

namespace ServerCrypt
{
    class program
    {
        static void Main(string[] args)
        {
            int Received = 0;
            int Read = 0;
            int Size = 1024;
            int Remaining = 0;

            TcpListener tcpListener = new TcpListener(IPAddress.Any, 8080);
            tcpListener.Start();
            Console.WriteLine("Server started");

            TcpClient tcpClient = tcpListener.AcceptTcpClient();
            Console.WriteLine("Connected to client");
            StreamReader reader = new StreamReader(tcpClient.GetStream());

            string Pass = reader.ReadLine();
            string cmdFileSize = reader.ReadLine();
            string cmdFileName = reader.ReadLine();
            string extractPath = cmdFileName;

            int length = Convert.ToInt32(cmdFileSize);
            byte[] buffer = new byte[length];
            
            Console.WriteLine("Receiving File");

            while (Received < length)
            {
                Remaining = length - Received;
                if (Remaining < Size)
                {
                    Size = Remaining;
                }

                Read = tcpClient.GetStream().Read(buffer, Received, Size);
                Received += Read;
            }

            using (FileStream fStream = new FileStream(Path.GetFileName(cmdFileName), FileMode.Create))
            {
                fStream.Write(buffer, 0, buffer.Length);
                fStream.Flush();
                fStream.Close();
            }
            Console.WriteLine("File received and saved in " + Environment.CurrentDirectory);

            DecryptFile(@".\Encrypted.zip", @".\Decrypted File.zip", Pass);

            ZipFile.ExtractToDirectory(@".\Decrypted File.zip", @".\Extracted File");

            Console.WriteLine("Your file is now decrypted");
            Console.WriteLine("Press Enter to Exit"); 
        }

        public static byte[] AES_Decrypt(byte[] bytesToBeDecrypted, byte[] Password)
        {
            byte[] decryptedBytes = null;

            byte[] saltBytes = new byte[] { 1, 2, 3, 4, 5, 6, 7, 8 };

            using (MemoryStream Memory = new MemoryStream())
            {
                using (RijndaelManaged AES = new RijndaelManaged())
                {
                    AES.KeySize = 256;
                    AES.BlockSize = 128;

                    var key = new Rfc2898DeriveBytes(Password, saltBytes, 1000);
                    AES.Key = key.GetBytes(AES.KeySize / 8);
                    AES.IV = key.GetBytes(AES.BlockSize / 8);

                    AES.Mode = CipherMode.CBC;

                    using (var cs = new CryptoStream(Memory, AES.CreateDecryptor(), CryptoStreamMode.Write))
                    {
                        cs.Write(bytesToBeDecrypted, 0, bytesToBeDecrypted.Length);
                        cs.Close();
                    }
                    decryptedBytes = Memory.ToArray();
                }
            }
            return decryptedBytes;
        }

        public static void DecryptFile(string fileEncrypted, string file, string password)
        {
            byte[] bytesToBeDecrypted = File.ReadAllBytes(fileEncrypted);
            byte[] passwordBytes = Encoding.UTF8.GetBytes(password);
            passwordBytes = SHA256.Create().ComputeHash(passwordBytes);

            byte[] bytesDecrypted = AES_Decrypt(bytesToBeDecrypted, passwordBytes);

            File.WriteAllBytes(file, bytesDecrypted);
        }
    }
}